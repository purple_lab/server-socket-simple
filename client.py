import os
import socket

def start():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.connect(('localhost', 5555))
    print(" connected to the server")
    resp = input(" Enter the data that must be sent to the server : ")
    server.send(resp.encode('utf-8'))
    confirmed = server.recv(255).decode('utf-8')
    print(confirmed)
    server.close()

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

clear()
start()