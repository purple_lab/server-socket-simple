import os
import socket

def start():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(("", 5555))
    server.listen(2)
    print(" server launched .")
    client = server.accept()
    INET = client[0]
    ADDR = client[1]
    data = INET.recv(255).decode("utf-8")
    print(" data received from{} : {}\n".format(ADDR, data))
    INET.send('ok'.encode('utf-8'))
    server.close()

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

clear()
start()
